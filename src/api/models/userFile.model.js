const mongoose = require("mongoose");

const schema = mongoose.Schema(
  {
    userId: {
      type: Number,
    },
    userEmail: {
      type: String,
    },
    imageId: {
      type: String,
    },
    fileUrl: {
      type: String,
    },
    fileType: {
      type: String,
    },
    category: {
      type: String,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("UserFile", schema, "UserFile");
