const express = require("express");
const controller = require("../../controllers/uploadController");
const router = express.Router();
const auth = require("../../middlewares/userAuth");

const multer = require("multer");

const storage = multer.memoryStorage({
  destination: function (req, file, callback) {
    callback(null, "");
  },
});

const upload = multer({
  storage,
  // limits: {
  //   fieldNameSize: 300,
  //   // fileSize: 52428800, // 50 Mb
  // },
}).single("image");

router.post("/upload", auth, upload, controller.addFile);

router.post("/upload-file", upload, controller.addMeetingVideo);

router.post("/competition-upload", auth, upload, controller.addCompetitionFile);

// router.post("/view-all", controller.viewAllFile);

router.get("/download/:fileName", controller.download);

// router.post("/delete", controller.deleteFile);

module.exports = router;
