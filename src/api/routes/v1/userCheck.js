const express = require("express");
const router = express.Router();

const controller = require("../../controllers/checkUser.controller");

router.post("/check", controller.check);

router.get("/user-submission", controller.getSubmissionCompetition);

module.exports = router;
