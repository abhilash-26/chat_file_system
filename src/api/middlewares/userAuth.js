const jwt = require("jsonwebtoken");
// const config = require("config");
module.exports = function (req, res, next) {
  const token = req.header("x-auth-token");
  if (!token) return res.status(401).send("Access Denied");
  try {
    // const user = jwt.verify(token, config.get("jwtPrivateKey"));
    const user = jwt.verify(token, "celebPrivateKey");
    req.user = user;
    next();
  } catch (err) {
    return res.status(400).send("Invalid Credintials");
  }
};
