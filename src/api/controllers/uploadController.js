const AWS = require("aws-sdk");
const uuid = require("uuid/v4");
// require("dotenv-safe").config();
const UserFile = require("../models/userFile.model");
const { sucessResponse, failResponse } = require("../utils/responseHandler");
const { awsId, awsKey, profileImage } = require("../../config/vars");
const logger = require("../../config/logger");
const axios = require("axios");

const s3 = new AWS.S3({
  accessKeyId: awsId,
  secretAccessKey: awsKey,
});

const saveData = async (
  fileUrl,
  imageId,
  userId,
  userEmail,
  imageType,
  fileName,
  category,
  res
) => {
  try {
    let message =
      category == ("celebrityschool-competition-digidrawing" || "profile")
        ? "Image uploaded sucessfully"
        : "Video uploaded sucessfully";

    const dataForDb = await UserFile.create({
      userId,
      userEmail,
      imageId,
      fileUrl,
      fileName,
      category,
      fileType: imageType.imageType,
    });
    if (category == "profile" || category == "Profile") {
      const updateProfilePic = await axios({
        method: "post",
        url: "https://www.origin.celebrityschool.in:1337/api/v1/user/update-user/profile-picture",
        data: {
          email: userEmail,
          profilePic: fileUrl,
        },
        headers: {
          "x-auth-token": "abc",
        },
      });
      if (updateProfilePic.data.meta.flag == "SUCCESS") {
        sucessResponse(res, dataForDb, 200, message);
        return;
      }
      failResponse(
        res,
        null,
        401,
        "failed to update profile pic please try again later"
      );
      return;
    }
    sucessResponse(res, dataForDb, 200, message);
  } catch (error) {
    failResponse(res, null, 401, error.message);
  }
};

const modifyData = async (
  userId,
  userEmail,
  oldPicture,
  category,
  imageId,
  fileUrl,
  res
) => {
  try {
    let message = "Image updated sucessfully";

    const updated = await UserFile.findOneAndUpdate(
      {
        userId,
        category,
      },
      {
        imageId,
        fileUrl,
      },
      { new: true }
    );
    if (category == "profile") {
      const updateProfilePic = await axios({
        method: "post",
        url: "https://www.origin.celebrityschool.in:1337/api/v1/user/update-user/profile-picture",
        data: {
          email: userEmail,
          profilePic: fileUrl,
        },
        headers: {
          "x-auth-token": "abc",
        },
      });
      console.log(updateProfilePic.data);
      if (updateProfilePic.data.meta.flag == "SUCCESS") {
        sucessResponse(res, updated, 200, message);
        return;
      }
      failResponse(
        res,
        null,
        401,
        "failed to update profile pic please try again later"
      );
      return;
    }
    sucessResponse(res, updated, 200, message);
  } catch (error) {
    failResponse(res, null, 401, error.message);
  }
};

exports.download = async (req, res) => {
  const fileName = req.params.fileName;
  try {
    const result = await s3
      .getObject({
        Bucket: bucket,
        Key: fileName,
      })
      .promise();
    res.send(result.Body);
  } catch (error) {
    failResponse(res, null, 401, error.message);
  }
};

/***********
  
adding or updating profile picture or banner image

************/

exports.addFile = async (req, res) => {
  try {
    const { category } = req.body;
    let userId = req.user.id;
    let userEmail = req.user.email;
    logger.info(`file size test- ${req.file.size}`);
    logger.info(`file size test- ${req.user.id}`);
    logger.info(`file size test- ${req.user.email}`);
    logger.info(`file size test- ${category}`);

    let fileUrl, imageId;
    const fileName = req.file.originalname;
    const imageType = req.file.mimetype;
    let myFile = req.file.originalname.split(".");
    const fileType = myFile[myFile.length - 1];
    let bucket = category;

    logger.info(`file name test- ${fileName}`);
    logger.info(`file imageType test- ${imageType}`);
    logger.info(`file my file test- ${myFile}`);
    logger.info(`file type- ${fileType}`);
    logger.info(`bucket type- ${bucket}`);

    // if (req.file.size > 52428800) {
    //   logger.info(`file size - ${req.file.size}`);
    //   failResponse(
    //     res,
    //     null,
    //     400,
    //     "file size too large upload file less than 50 mb"
    //   );
    //   return;
    // }
    if (category == "profile" || category == "cover" || category == "Profile") {
      logger.info(`inside`);
      bucket = "celebrity-school-profile";
    }
    // check for the file type
    // if (
    //   (category == "profile" || category == "cover" || category == "Profile") &&
    //   !profileImage.includes(imageType)
    // ) {
    //   logger.info("second check");
    //   failResponse(
    //     res,
    //     null,
    //     500,
    //     "profile picture format can only be jpg/png/jpeg"
    //   );
    //   return;
    // }
    logger.info(`bucket- ${bucket}`);

    const params = {
      Bucket: bucket,
      Key: `${uuid()}.${fileType}`,
      Body: req.file.buffer,
    };

    //check for the file size
    // if (
    //   (category == "profile" || category == "cover") &&
    //   req.file.size > 1 * 1024 * 1024
    // ) {
    //   failResponse(res, null, 500, "file size too large");
    //   return;
    // }

    if (userId && category) {
      let oldPicture;
      if (category == "profile" || category == "cover") {
        oldPicture = await UserFile.findOne({
          userId,
          category,
        });
      }

      // if image is already there then delete from the aws bucket
      if (oldPicture) {
        const result = await s3
          .deleteObject({ Bucket: bucket, Key: oldPicture.imageId })
          .promise();
      }

      // upload to the aws bucket
      s3.upload(params, (error, data) => {
        if (error) {
          failResponse(res, null, 500, error);
        }
        fileUrl = data.Location;
        imageId = data.key;
        // console.log(data);
        // logger.info(`file size test- ${data.Location}`);
        // logger.info(`file size test- ${data.key}`);

        // if image is already there then modify the image data in databse
        if (oldPicture) {
          modifyData(
            userId,
            userEmail,
            oldPicture,
            category,
            imageId,
            fileUrl,
            res
          );
          return;
        }

        // saving the data to the database
        saveData(
          fileUrl,
          imageId,
          userId,
          userEmail,
          { imageType: imageType },
          fileName,
          category,
          res
        );
      });
      return;
    }
    failResponse(res, null, 401, "user id and category is required");
  } catch (error) {
    failResponse(res, null, 401, error.message);
  }
};

// exports.viewAllFile = async (req, res) => {
//   try {
//     const allFiles = await s3.listObjectsV2({ Bucket: bucket }).promise();
//     res.send(allFiles);
//   } catch (error) {
//     console.log(error);
//   }
// };

exports.deleteFile = async (req, res) => {
  try {
    // const fileName = req.params.fileName;
    const { userId, category, fileName } = req.body;
    let bucket = category;

    if (userId && (category == "profile" || category == "cover")) {
      const user = await UserFile.findOneAndDelete({
        userId: userId,
        category: category,
      });
      if (user == null) {
        failResponse(
          res,
          null,
          401,
          "No image found with given user id and category"
        );
        return;
      }
      await s3.deleteObject({ Bucket: bucket, Key: user.imageId }).promise();

      sucessResponse(res, null, 200, "image deleted sucessfully");
      return;
    }
    if (fileName) {
      const user = await UserFile.findOneAndDelete({
        imageId: fileName,
      });
      if (user == null) {
        failResponse(res, null, 401, "No image found with given image id");
      } else {
        await s3.deleteObject({ Bucket: bucket, Key: fileName }).promise();
        sucessResponse(res, null, 200, "image deleted sucessfully");
      }
    } else {
      failResponse(res, null, 401, "image id missing");
    }
  } catch (error) {
    failResponse(res, null, 401, error.message);
  }
};

exports.addMeetingVideo = async (req, res) => {
  try {
    const { category } = req.body;

    let fileUrl, imageId;
    const fileName = req.file.originalname;
    const imageType = req.file.mimetype;
    let myFile = req.file.originalname.split(".");
    const fileType = myFile[myFile.length - 1];
    let bucket = category;

    const params = {
      Bucket: bucket,
      Key: `${uuid()}.${fileType}`,
      Body: req.file.buffer,
    };

    // upload to the aws bucket
    s3.upload(params, (error, data) => {
      if (error) {
        return failResponse(res, null, 500, error);
      }
      fileUrl = data.Location;
      imageId = data.key;
      dataTosend = {
        fileUrl: fileUrl,
      };
      sucessResponse(res, dataTosend, 200, "File uploaded successfully");
    });
  } catch (error) {
    failResponse(res, null, 401, error.message);
  }
};

exports.addCompetitionFile = async (req, res) => {
  try {
    const { category, albumId, description } = req.body;
    let userEmail = req.user.email;
    let fileUrl, imageId;
    const fileName = req.file.originalname;
    const imageType = req.file.mimetype;
    let myFile = req.file.originalname.split(".");
    const fileType = myFile[myFile.length - 1];
    let bucket = category;

    const params = {
      Bucket: bucket,
      Key: `${uuid()}.${fileType}`,
      Body: req.file.buffer,
    };

    // upload to the aws bucket
    s3.upload(params, (error, data) => {
      if (error) {
        return failResponse(res, null, 500, error);
      }

      fileUrl = data.Location;
      imageId = data.key;
      saveSubmission(res, albumId, fileUrl, userEmail, description);
    });
  } catch (error) {
    failResponse(res, null, 401, error.message);
  }
};

async function saveSubmission(res, albumId, fileUrl, userEmail, description) {
  try {
    const submission = await axios({
      method: "post",
      url: "https://chat.celebrityschool.in/v1/competition-submission/upload-submission",
      data: {
        albumId: albumId,
        fileUrl: fileUrl,
        userEmail: userEmail,
        description: description,
      },
    });
    res.send(submission.data);
  } catch (error) {
    failResponse(res, null, 400, error.message);
  }
}

////////////////////////////////////
