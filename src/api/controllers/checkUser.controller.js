const UserFile = require("../models/userFile.model");
const { sucessResponse, failResponse } = require("../utils/responseHandler");

exports.check = async (req, res) => {
  try {
    const { userId, category } = req.body;
    const result = await UserFile.findOne({
      userId: userId,
      category: category,
    });
    sucessResponse(res, result, 200, "found detail is");
  } catch (error) {
    failResponse(res, null, 401, error.message);
  }
};

exports.getSubmissionCompetition = async (req, res) => {
  try {
    // let limit = req.query.limit;
    // let searchvalue = req.query.searchvalue;
    // let sortBy = req.query.sortBy || "";
    // let sort = req.query.sort;
    // let reg = "";
    // if (searchvalue) {
    //   reg = searchvalue;
    // }

    // const allFilterSearch = {
    //   $or: [
    //     {
    //       userEmail: {
    //         $regex: reg,
    //         $options: "i",
    //       },
    //     },
    //     // {
    //     //   category: {
    //     //     $regex: reg,
    //     //     $options: "i",
    //     //   },
    //     // },
    //   ],
    // };
    // let limitTo = parseInt(limit);
    // const page = req.query.page;
    // let skip = parseInt(limit) * (parseInt(page) - 1);
    // let result = [];

    // if (sortBy.trim() !== "")
    //   result = await PushNotification.find(allFilterSearch)
    //     .sort({ [sortBy === "ID" ? "_id" : sortBy]: sort === "true" ? 1 : -1 })
    //     .limit(limitTo)
    //     .skip(skip);
    // else
    //   result = await PushNotification.find(allFilterSearch)
    //     .limit(limitTo)
    //     .skip(skip);

    const { category } = req.query;
    if (!category) return failResponse(res, null, 401, "Category is required");
    const allSubmission = await UserFile.find({
      category,
    });
    // .sort({ [sortBy === "ID" ? "_id" : sortBy]: sort === "true" ? 1 : -1 })
    // .limit(limitTo);
    // .skip(skip);

    const count = await UserFile.countDocuments({
      category,
    });

    let responseData = {
      data: allSubmission,
      count: count,
    };

    sucessResponse(res, responseData, 200, "found detail is");
  } catch (error) {
    failResponse(res, null, 401, error.message);
  }
};
